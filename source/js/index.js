import '../css/style.scss'

import $ from 'jquery'
import 'slick-carousel'
import ViewPort from './modules/module.viewport'
import Tab from './modules/module.tab'
import './modules/module.form'
import './modules/module.helper'
import Popup from './modules/module.popup'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min'
import clipboard from 'clipboard-copy'

import DrugNDrop from './modules/module.drugndrop'

window.app = window.app || {};

$(()=>{

	new Tab();

	let $b = $('body');


	window.app.popup = new Popup({
		onPopupClose:(pop)=>{
			$b.removeClass('is-popup-calc');

		},
		onPopupOpen:(pop)=>{
			$b.removeClass('show-menu');

			if ( $(pop).hasClass('popup_calculator')){
				$b.removeClass('is-popup');
				$b.addClass('is-popup-calc');
			}
		}
	});


	let breakpoint, isMobile;

	new ViewPort({
		'0': ()=>{
			breakpoint = 'mobile';
			isMobile = true;
			if (window.app.init) window.app.init();
		},
		'800': ()=>{
			breakpoint = 'desktop';
			isMobile = false;
			if (window.app.init) window.app.init();
		}
	});

	$b
		.on('click', '[data-scroll]', function(e){
			e.preventDefault();
			let offset = isMobile ? 140 : 20;
			$('html,body').animate({scrollTop: $($(this).attr('data-scroll')).offset().top - offset}, 500);
		})
		.on('click', '.header__filter', function(e){
			e.preventDefault();
			window.app.popup.close();
			$b.toggleClass('show-filter')
		})
		.on('click', '.header__handler', function(e){
			e.preventDefault();
			$b.toggleClass('show-menu')
		})
		.on('click', '.catalog__info-view', function(e){
			e.preventDefault();
			let $t = $(this),
				$prod = $('.catalog__list')
			;

			if ( $t.hasClass('is-active') ) return false;
			$t.addClass('is-active').siblings().removeClass('is-active');
			$prod.toggleClass('catalog__list_grid catalog__list_lines');


		})
		.on('click', '.product__preview-item', function(e){

			let $t = $(this),
				$wrap = $t.closest('.product__preview'),
				$view = $wrap.find('.product__preview-image').find('img'),
				$link = $wrap.find('.product__preview-open')
			;

			$wrap.find('.product__preview-item').removeClass('is-active');
			$t.addClass('is-active');
			$view.attr('src', $t.attr('data-src'));
			$link.attr('href', $t.attr('data-src'));


		})
		.on('click', '.product__gallery-thambnail', function(e){

			let $t = $(this),
				$wrap = $t.closest('.product__gallery'),
				$view = $wrap.find('.product__gallery-main-img').find('img'),
				$link = $wrap.find('.product__gallery-main-open')
			;

			$wrap.find('.product__gallery-thambnail').removeClass('is-active');
			$t.addClass('is-active');
			$view.attr('src', $t.attr('data-src'));
			$link.attr('href', $t.attr('data-src'));


		})
		.on('click', '.product__accordion-head', function(e){

			let $t = $(this),
				$wrap = $t.closest('.product__accordion'),
				$body = $wrap.find('.product__accordion-body')
			;

			if ($t.hasClass('is-buzy')) return false;
			$t.addClass('is-buzy');
			$body.slideToggle(300, ()=>{
				$wrap.toggleClass('is-open');
				$body.removeAttr('style');
				$t.removeClass('is-buzy');
				$('.slick-slider').slick('setPosition');
			})

		})
		.on('click', '[data-popup-page]', function(e){

			e.preventDefault();

			let $t = $(this),
				url = $t.attr('href')
			;

			$.ajax({
				url: url,
				cache: false
			}).done(function(data){

				window.app.popup.open('.popup_product', data);


				// let popup = window.app.popup.open('.popup_product');
				// $(popup).find('.popup__content').html(data);

				setTimeout(window.app.init, 100);
			}).fail(function(err){
			});

		})
		.on('click', '[data-clipboard]', function(e){
			clipboard($(this).attr('data-clipboard'));
		})
	;


	let plugin = {
		productPreviewCarousel:()=>{
			$('.product__preview-carousel').each(function(){
				let $slider = $(this),
					$view = $slider.closest('.product__preview').find('.product__preview-image').find('img'),
					options = {
						infinite: false,
						speed: 300,
						swipe: true,
						swipeToSlide: true,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						// fade: true,
						adaptiveHeight: true,
						prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="88" height="88" viewBox="0 0 88 88"><path d="M580.99,1281.01l18,17.75H588.124V1316H572.993v-17.24h-10Z" transform="translate(-537 -1254.5)"/></svg></div>',
						nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="88" height="88" viewBox="0 0 88 88"><path d="M580.99,1281.01l18,17.75H588.124V1316H572.993v-17.24h-10Z" transform="translate(-537 -1254.5)"/></svg></div>'
					}
				;

				try{
					$slider.slick('unslick')
				} catch (e) {}

				if ( isMobile ){
					options.rows = 2;
					options.slidesPerRow = 2;
				} else {
					options.swipe = false;
					options.swipeToSlide = false;
					options.vertical = true;
					options.slidesToShow = 2;
				}

				$slider
					.on('init', function (event, slick) {
						// let $active = $(slick.$slides[0]).find('.product__preview-item').eq(0);
						// $active.addClass('is-active');
						// $view.attr('src', $active.attr('data-src'))
					})
					.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

					})
					.on('afterChange', function () {

					})
					.slick(options);

			});


			$('.product__accordion-gallery').each(function(){
				let $slider = $(this),
					options = {
						infinite: false,
						speed: 300,
						swipe: true,
						swipeToSlide: true,
						arrows: true,
						dots: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						// fade: true,
						adaptiveHeight: true,
						prevArrow: '<div class="slick-arrow slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="44" height="44" viewBox="0 0 44 44"><path d="M29.913,26.97H14.069l7.922-10.563L29.913,26.97m4.076,2.038-12-16-12,16h24Z" /></svg></div>',
						nextArrow: '<div class="slick-arrow slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="44" height="44" viewBox="0 0 44 44"><path d="M29.913,26.97H14.069l7.922-10.563L29.913,26.97m4.076,2.038-12-16-12,16h24Z" /></svg></div>'
					}
				;

				try{
					$slider.slick('unslick')
				} catch (e) {}

				$slider
					.on('init', function (event, slick) {

					})
					.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

					})
					.on('afterChange', function () {

					})
					.slick(options);

			});
		},

		scrollBar: ()=>{
			$('.scrollbar').each(function () {
				let $t = $(this);

				if ( !$t.hasClass('is-inited') && !isMobile ){
					$t.addClass('is-inited');
					$t.mCustomScrollbar();
				} else if ( $t.hasClass('is-inited') && isMobile ){
					$t.removeClass('is-inited');
					$t.mCustomScrollbar('destroy');
				}
			});
		},

		fixedProductAside: (clear)=> {

			$('.product__about').each(function () {
				let $t = $(this),
					$popup = $t.closest('.popup'),
					$info = $popup.find('.product__info'),
					$infoInner = $info.find('.product__info-inner'),
					$parent = $popup.find('.product'),
					$container = $popup.find('.popup__inner'),
					info = {}
				;

				info.height = $infoInner.outerHeight(true);

				$container.on('scroll', (e)=>{

					let parent = $parent[0].getBoundingClientRect();

					let isTopOut,
						isBottomOut;

					if ( parent.height > window.innerHeight ){

						if (info.height < window.innerHeight){
							isTopOut = parent.y < 0;
							isBottomOut = parent.height + parent.y - info.height < 0;


							if ( isTopOut && !isBottomOut){
								fixed(false);
								fixed('top');
							} else if (isTopOut && isBottomOut){
								fixed(false);
								fixed('down');
							} else {
								fixed(false);
							}
						} else {
							isTopOut = parent.y + info.height - window.innerHeight < 0;
							isBottomOut = parent.height + parent.y - window.innerHeight < 0;

							if ( isTopOut && !isBottomOut){
								fixed(false);
								fixed('bottom');
							} else if (isTopOut && isBottomOut){
								fixed(false);
								fixed('down');
							} else {
								fixed(false);
							}
						}
					}

				});

				function fixed(pos) {
					if (pos === 'top' && !$infoInner.hasClass('fixed_top')){
						$infoInner.addClass('fixed_top');

					} else if (pos === 'bottom' && !$infoInner.hasClass('fixed_bottom') ){
						$infoInner.addClass('fixed_bottom');

					} else if (pos === 'down' && !$infoInner.hasClass('fixed_down')){
						$infoInner.addClass('fixed_down');

					} else if ( pos === false ){
						$infoInner.removeClass('fixed_bottom fixed_top fixed_down');
					}

				}

			})
		},

		clipboard: ()=>{
			$('[data-clipboard]').each(function () {
				let $t = $(this);

				if ( $t.hasClass('is-clipped')) return false;
				$t.addClass('is-clipped');

				new ClipboardJS($t[0], {

				})
			})
		},

		dropDown: ()=>{
			$('.drugndrop__box').each(function () {
				let $t = $(this),
					$box = $t.find('.drop-case')
				;

				if ( $t.hasClass('is-inited')) return false;
				$t.addClass('is-inited');

				new DrugNDrop({
					element: $t[0],
					output: $box[0],
					fileInputId: 'drugndropFileInput',
					fileInputName: 'file',
					maxFiles: 10,
					maxFileSize: 15000000,
					autoupload: true,
					url: 'http://test.maestros.ru/test.php',
					itemTemplate: (item)=>{

						let
							image = item.src ? '<div class="drugndrop__item-image"><img src="{src}"></div>' : '',
							size = item.size.toString().length > 6 ? item.size.toString().slice(0,-6) + 'Мбайт' : item.size.toString().length > 3 ? item.size.toString().slice(0,-3) + 'Кбайт' : item.size,
							progress = item.progress ? `{progress}
								<div class="drugndrop__progress">
									<div class="drugndrop__progress-value"></div>
									<div class="drugndrop__progress-bar"></div>
								</div>` : ''
							// extension = item.name.match(/\w+/ig).pop(),
							// name = item.name.replace(`.${extension}`, '')
						;

						return `<div class="drugndrop__item">
							${progress}
							<div class="drugndrop__item-inner">
								<div class="drugndrop__item-remove" data-remove="{id}"></div>
								<div class="drugndrop__item-name">{name} (${size})</div>
							</div>
						</div>`
					},
					onFileAdded:(a, b)=>{
						//console.info(a, b)

					},
					onOverSize:(file)=>{
						//console.info(a, b)
						// window.app.popup.open('.popup_message', `<div class='popup__text'>Превышен допустимый размер файла: ${file}</div>`)

					},
					onOverLimit:(a, b)=>{
						//console.info(a, b)
						// window.app.popup.open('.popup_message', `<div class='popup__text'>Достигнут допустимый лимит файлов - 10 шт.</div>`)
					},
					onFileUploaded:(id, item)=>{
						$(item).addClass('is-uploaded')
					}
				});


			});
		}
	};

	window.app.init = ()=>{
		plugin.productPreviewCarousel();
		plugin.dropDown();
		plugin.scrollBar();
		window.setInputMask();
		plugin.fixedProductAside();


		/*let timer = null;

		$(window).on('resize', ()=>{
			clearTimeout(timer);
			timer = setTimeout(()=>{
				plugin.fixedProductAside('clear');
				plugin.fixedProductAside();
			}, 300);
		})*/
	};



	window.app.init();

});



ymaps.ready(function () {

	if ( document.querySelectorAll('#contactMap').length ){
		let map = new ymaps.Map('contactMap', {
				center: [59.788665, 30.489261],
				zoom: 10,
				controls: ['zoomControl']
			}),
			myPlacemark = new ymaps.Placemark([59.788665, 30.489261],
				{},
				{
					iconLayout: 'default#image',
					iconImageHref: '../img/marker.png',
					iconImageSize: [56, 84],
					iconImageOffset: [-28, -84]
				});
		map.geoObjects.add(myPlacemark);

		map.behaviors.disable('scrollZoom');
		map.behaviors.disable('multiTouch');

		// map.behaviors.disable('drag');

		new ViewPort({
			'0': ()=>{
				map.setCenter([59.78, 30.48], 10);
			},
			'800': ()=>{
				map.setCenter([59.78, 30.78], 10);
			}
		});
	}


});

