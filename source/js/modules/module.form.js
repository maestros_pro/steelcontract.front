import $ from 'jquery'
import Forms from './module.validate'
import Inputmask from 'inputmask'

window.app = window.app || {};

$(()=>{

	/*let data = [
		{
			name: 'password',
			error: true,
			success: true,
			message: 'фвфывфы'
		}
	];*/

	let scrollTimer = null;

	class Form extends Forms {
		result (data){
			for (let i = 0; i < data.length; i ++){

				let $input = $(`[name = "${data[i].name}"]`),
					$field = $input.closest('.form__field'),
					$message = $field.find('.form__message')
				;

				if (!$message.length){
					$message = $('<div class="form__message"></div>').appendTo($field.eq($field.length - 1));
				}

				if (data[i].error) $field.addClass('f-error');
				if (data[i].success) $field.addClass('f-success');
				if (data[i].message) {
					$field.addClass('f-message');
					$message.html(data[i].message)
				}
			}

			if ($('.f-error').length) {

				clearTimeout(scrollTimer);
				scrollTimer = setTimeout(()=>{
					let offset = window.app.breakpoint === 'mobile' ? 140 : 20;
					$('html,body').animate({scrollTop: $('.f-error').eq(0).offset().top - offset}, 500);

				}, 200);

			}
		}

		checkFilled(input){

			let $input = $(input),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( input.type !== 'radio' && input.type !== 'checkbox' ) {
				if ( !$.trim($input.val()).length ){
					$field.removeClass('f-filled');
				} else {
					$field.addClass('f-filled');
				}
			} else if ( $input.closest('.form__select').length ) {
				if ( $field.find(`input[name="${input.name}"]`).filter(':checked').length ){
					$field.addClass('f-filled');
				} else {
					$field.removeClass('f-filled');
				}

				if (input.checked) {
					let $value = $field.find('.form__select-value'),
						value = $input.closest('.form__select-item').find('.form__select-option').html()
					;

					$value.html(value);
					$field.removeClass('f-select')

				}
			}


		}
	}

	const form = new Form();

	window.setInputMask = function (){

		$('[data-mask]').each(function () {
			let $t = $(this);

			console.info($t, $t.hasClass('is-masked'));

			if ( !$t.hasClass('is-masked') ) {
				$t.addClass('is-masked');

				let inputmask = new Inputmask({
					mask: $t.attr('data-mask'),		//'+7 (999) 999-99-99',
					showMaskOnHover: false,
					onincomplete: function() {
						this.value = '';
						$t.closest('.form__field').removeClass('f-filled');
					}
				});

				inputmask.mask($t[0]);
			}
		});
	};

	$('.form__field input').each(function(e){
		if (this.name) form.checkFilled(this);
	});

	$('body')
		.on('mousedown', (e)=>{
			if ( $('.form__field.f-select').length ){
				if ( !$(e.target).closest('.form__field.f-select').length ) $('.form__field').removeClass('f-select');
			}
		})
		.on('click', '.form__select-value', function (e) {
			let $t = $(this),
				$field = $t.closest('.form__field')
			;

			if ($field.hasClass('f-disable')) return false;
			$field.addClass('f-select')

		})
		.on('click', '.js-clear-form', function (e) {
			e.preventDefault();
			let $t = $(this),
				$form = $t.closest('form'),
				$input = $form.find('input')
			;

			$input.each(function () {
				let $t = $(this);
				if ( $t.attr('type') === 'radio' || $t.attr('type') === 'checkbox' ){
					$t.prop('checked', false);
				} else {
					$t.val('');
				}
				$t.closest('.form__field').removeClass('f-filled f-error f-message').find('.form__select-value').html('');
			});
		})
		.on('click', '.form__select-option', function(e){
			let $t = $(this),
				$item = $t.closest('.form__select-item'),
				$input = $item.find('input[type=radio]')
			;

			if ( $input.prop('checked') ){
				e.preventDefault();
				$input.prop('checked', false);
				$t.closest('.form__field').removeClass('f-filled f-error f-message f-select').find('.form__select-value').html('');
			}

		})
		.on('click', '.form__input-clear', function (e) {
			let $t = $(this),
				$field = $t.closest('.form__field'),
				$input = $field.find('input')
			;
			$input.val('');
			$field.removeClass('f-filled f-error f-message')
		})
		.on('click', '.form__input-arrow', function (e) {
			let $t = $(this),
				$field = $t.closest('.form__field'),
				$input = $field.find('input')
			;
			$input.focus();

		})
		.on('mousedown', '.form__select-item', function (e) {
			let $t = $(this),
				$field = $t.closest('.form__field'),
				$inputText = $field.find('.form__input-value'),
				$inputValue = $field.find('.form__input-select'),
				value = $t.attr('data-value'),
				text = $t.html()
			;
			$inputText.html(text);
			$inputValue.val(value);
		})

		.on('blur change', '.form__field input, .form__field textarea', (e)=>{
			form.checkFilled(e.target);
		})

		.on('change', '[data-require]', (e) =>{

			let $input = $(`[name = "${e.target.name}"]`),
				$field = $input.closest('.form__field');

			$field
				.removeClass('f-error f-message f-success')
				.find('.form__message')
				.html('');
		})

		.on('submit', '.form-validate', function (e) {
			let $form = $(this),
				$item = $form.find('input, textarea').filter(function(){return $(this).is(':visible')}),
				wrong = false
			;

			$item.each(function () {
				let input = $(this), rule = $(this).attr('data-require');

				if ( !input.is(':visible') ) return false;

				$(input)
					.closest('.form__field')
					.removeClass('f-error f-message f-success')
					.find('.form__message')
					.html('')
				;

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							let data = [{
								name: input[0].name,
								error: true,
								message: err.errors[0]
							}];

							form.result(data);

						}
					})
				}
			});
			if ( wrong ){
				e.preventDefault();
			} else if ( $form.hasClass('js-calculate')){
				e.preventDefault();

				let data = $form.serializeArray(),
					calc = {}
				;

				for (let i = 0; i < data.length; i++){
					calc[data[i].name] = data[i].value
				}

				let msg = `Вес трубы ${calc.calcD}x${calc.calcS} длиной ${calc.calcL}м:
						<br/>
						<b>${calcTruba(calc.calcD,calc.calcS,calc.calcL,calc.calcM)} кг</b>
						<br/><br/>
						Плотность материала ρ:
						<br/>
						<b>${calc.calcM} кг/м<sup>3</sup></b>
						<br/><br/>
						Расчет производился по формуле:<br />
						m = Pi * ρ * S * (D - S) * L`;

				function calcTruba(d, s, l, ro) {
					s = s * 0.001;
					d = d * 0.001;

					let res = Math.PI * ro * s * (d - s) * l;

					res = res.toFixed(2);
					return res;
				}

				$form.find('.js-calc-result').html(msg);
			}
		})
		.on('click', '.form__clone', function (e) {
			let $t = $(this),
				max = 5,
				$field = $t.closest('.form__field'),
				$group = $t.closest('.form__clonegroup'),
				name = $field.find('input').attr('name').replace(/\[(\w)+\]/ig, ''),
				isLast = $field.index() === $group.find('.form__field').length - 1
			;

			if ( isLast && $group.find('.form__field').length < max ){
				let $clone = $(`<div class="form__field">${$field.html()}</div>`);
				$clone.find('input').removeClass('is-masked').val('');

				$group.append($clone);
				window.setInputMask();
			} else{
				$field.remove();
			}

			$group.find(`[name^=${name}]`).each(function (i) {
				$(this).attr('name', `${name}[${i}]`);
			});

		})
	;


});