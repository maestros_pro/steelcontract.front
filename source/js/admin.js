import '../css/admin.scss'

import 'bootstrap'
import $ from "jquery";


$(()=>{


	$('body').on('change', '.js-file-input', function (e) {
		let $t = $(this),
			$group = $t.closest('.form-group'),
			$field = $t.closest('.form-image'),
			name = $t.attr('name').replace(/\[(\w)+\]/ig, ''),
			$clone = $(`<div class="form-image">${$field.html()}</div>`),
			collect = e.dataTransfer || e.target,
			file = collect.files[0]
		;


		if ( /^(image)/ig.test(file.type) ){
			let reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onloadend = ()=>{

				$field.prepend(`
					<div class="form-image-img">
						<img src="${reader.result}">
						<div class="btn btn-sm btn-danger js-file-delete">Удалить</div>
					</div>
					<div class="form-image-name">${file.name}</div>
					
				`);

				$group.append($clone);

				$group.find(`[name^=${name}]`).each(function (i) {
					$(this).attr('name', `${name}[${i}]`);
				});

			};
		} else {
			alert('Загрузить можно только файлы изображений')
		}


	})
		.on('click', '.js-file-delete', function () {
			$(this).closest('.form-image').remove();
		})

});
